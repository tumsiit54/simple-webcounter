FROM python:3.4-alpine
ADD . /app_dir
WORKDIR /app_dir
RUN pip install -r requirements.txt
CMD ["python", "main.py"]
